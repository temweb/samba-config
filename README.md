Hi,

Please run these commands in order. It will setup the samba server, using the configuration as described on the file config-samba. (Please adjust the networking settings accordingly). 

Make sure that you setup an installation repo prior to run this command. Also make sure to use a VirtualMachine, so in case you have any issues you can just try again. 

#yum install -y git
#git clone https://temweb@bitbucket.org/temweb/samba-config.git
#bash samba-config/samba_server_setup

You will be asked to enter the password for user01. Please use “password” as the password for your testing environment. 

Lastly, you should be able to see the share using another VM with the command:

#smbclient //<enter the samba IP or hostname>/shared -U user01%password

PLease let me know how this configuration goes. Again, this is outside the scope of this course. 